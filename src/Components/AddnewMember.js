import React from 'react'
import { useState } from "react";
import { Link } from "react-router-dom";

function AddnewMember() {

      const [name, setName] = useState("");
      const [idno, setIdno] = useState("");
      const [phone, setPhone] = useState("");
      const [address, setAddress] = useState("");
      const [isPending, setIspending] = useState(false);

      const handleSubmit = (e) => {
        e.preventDefault();
        setIspending(true);

        setName(e.target.value);
        setIdno(e.target.value);
        setPhone(e.target.value);
        setAddress(e.target.value);
       

        console.log(name);
        console.log(idno);
        console.log(phone);
        console.log(address);
        
        console.log("new blog added");
        setIspending(false);
      };

  return (
    <div className="form">
      <h2>+Add New Member</h2>
      <Link to="/">
        <p>Back home</p>
      </Link>
      <form onSubmit={handleSubmit}>
        <div className="input">
          <label>Name:</label>
          <input value={name} onChange={(e) => setName(e.target.value)} />
           
         
        </div>
        <div className="input">
          <label>ID No: </label>
          <input value={idno} onChange={(e) => setIdno(e.target.value)}/>
            
        </div>
        <div className="input">
          <label>Phone:</label>
          <input
          type='phone'
            value={phone}
            onChange={(e) => setPhone(e.target.value)}
          />
          
        </div>
        <div className="input">
          <label>Address: </label>
          <input
            type="text"
            value={address}
            onChange={(e) => setAddress(e.target.value)}
          />
        </div>
       
        {!isPending && <button type="submit">Submit</button>}
        {isPending && <button>Adding member...</button>}
      </form>
    </div>
  );
}

export default AddnewMember