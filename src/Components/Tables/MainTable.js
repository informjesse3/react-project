import React, { useEffect, useState } from "react";
import axios from "axios";

function MainTable() {
  const [data, setData] = useState([]);
   const [nav, setNav] = useState(false);
   const [openConfirm, setOpenConfirm] = useState(false)

const handeConfirm = () => {
  setOpenConfirm(!openConfirm)
}

   function handleNav() {
     setNav(!nav);
     console.log("set changed");
   }
   const [formData, setFormData] = useState({
     loadNo: "",
     firstName: "",
     lastName: "",
     dropOff: "",
     time: "",
     date: "",
     driver: "",
   });

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get("http://localhost:3001/infor");
        console.log("My Data", response.data)
        setData(response.data);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchData();
  }, []);
  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await axios.post(
        "http://localhost:3001/infor",
        formData
      );
      console.log("Response:", response.data);
      // If successful, fetch updated data
      
      // Clear form data
      setFormData({
        loadNo: "",
        firstName: "",
        lastName: "",
        dropOff: "",
        time: "",
        date: "",
        driver: "",
      });
    } catch (error) {
      console.error("Error creating data:", error);
    }
    handleNav();
    window.location.reload();
  };

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleDelete = async (itemId) => {
    try {
      // Send a DELETE request to your API to delete the item
      await axios.delete(`http://localhost:3001/infor/${itemId}`);

      // If deletion is successful, update the data state to reflect the change
      setData((prevData) => prevData.filter((item) => item._id !== itemId));
    } catch (error) {
      console.error("Error deleting data:", error);
    }
  };

  

  return (
    <>
      {openConfirm && (
        <div className="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 flex gap-2 bg-gray-200 py-4 shadow-xl shadow-gray-700 border boredr-gray-500 w-[500px] h-[150px] z-50 justify-center text-center rounded-lg">
          <h1>Are you Sure?</h1>
          <div className="space-x-2">
            <button
              onClick={(item) => handleDelete(item._id)}
              className="text-sm bg-[#001b5e] text-white p-1 rounded-lg hover:opacity-90 duration-200 ease-linear hover:scale-110 "
            >
              Yes
            </button>
            <button
              onClick={handeConfirm}
              className="text-sm bg-[#7d869c] text-white p-1 rounded-lg hover:opacity-90 duration-200 ease-linear hover:scale-110 "
            >
              No
            </button>
          </div>
        </div>
      )}
      <div className="table1">
        <h2>KHFHG</h2>
        <table>
          <thead>
            <tr>
              <th>LJSJJ</th>
              <th>OITWRR</th>
              <th>FAF</th>
              <th>TSFS</th>
              <th>DaStSFe</th>
              <th>DriSFver</th>
              <th>
                <div className="flex flex-row gap-1">
                  <div
                    className="bg-[#f03232] w-3 h-3 mt-3 rounded-md cursor-pointer"
                    onClick={handleNav}
                  />
                  <h1>Action</h1>
                </div>
              </th>
            </tr>
          </thead>
          <tbody>
            {data.map((item) => (
              <tr key={item._id}>
                <td>{item.firstName}</td>
                <td>{item.lastName}</td>
                <td>{item.dropOff}</td>
                <td>{item.time}</td>
                <td>{item.date}</td>
                <td>{item.driver}</td>
                <td className="text-center">
                  <button
                    // onClick={() => handleDelete(item._id)}
                    onClick={() => handeConfirm()}
                    className="text-sm bg-[#001b5e] text-white p-1 rounded-lg hover:opacity-90 duration-200 ease-linear hover:scale-110 "
                  >
                    delete
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
        {nav && (
          <form
            onSubmit={handleSubmit}
            className="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 flex gap-2 bg-gray-200 py-4 shadow-xl shadow-gray-700 border boredr-gray-500 w-[500px] z-50 justify-center text-center"
          >
            <div className="flex flex-col gap-2">
              <div className="flex flex-row justify-between space-x-8">
                <h1 className="font-bold">
                  JKSFJK{" "}
                  <span
                    className="text-red-500 cursor-pointer"
                    onClick={handleNav}
                  >
                    {"{close}"}
                  </span>
                </h1>
              </div>
              <input
                type="text"
                name="loadNo"
                value={formData.loadNo}
                onChange={handleChange}
                placeholder="Load No."
                className="border border-gray-400 p-2 rounded shadow-lg"
              />
              <input
                type="text"
                name="driver"
                value={formData.driver}
                onChange={handleChange}
                placeholder="Driver"
                className="border border-gray-400 p-2 rounded shadow-lg"
              />
              <input
                type="text"
                name="time"
                value={formData.time}
                onChange={handleChange}
                placeholder="Time"
                className="border border-gray-400 p-2 rounded shadow-lg"
              />
              <input
                type="text"
                name="firstName"
                value={formData.firstName}
                onChange={handleChange}
                placeholder="First Name"
                className="border border-gray-400 p-2 rounded shadow-lg"
              />
            </div>
            <div className="flex flex-col gap-2 mt-8">
              {" "}
              <input
                type="text"
                name="lastName"
                value={formData.lastName}
                onChange={handleChange}
                placeholder="Last Name"
                className="border border-gray-400 p-2 rounded shadow-lg"
              />
              <input
                type="text"
                name="dropOff"
                value={formData.dropOff}
                className="border border-gray-400 p-2 rounded shadow-lg"
                onChange={handleChange}
                placeholder="Drop Off"
              />
              <input
                type="text"
                name="date"
                value={formData.date}
                onChange={handleChange}
                placeholder="Date"
                className="border border-gray-400 p-2 rounded shadow-lg"
              />
              <button
                type="submit"
                className="bg-[#001b5e] text-white p-1/2 rounded hover:scale-110 duration-200 ease-linear"
              >
                Add
              </button>
            </div>
          </form>
        )}
      </div>
    </>
  );
}

export default MainTable;
