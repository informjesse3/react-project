export const COLUMNS = [
  {
    Header: "Load Number",
    accessor: "id",
  },
  {
    Header: "Pick Up",
    accessor: "pick_up",
  },
  {
    Header: "Drop Off",
    accessor: "drop_off",
  },
  {
    Header: "Driver",
    accessor: "driver",
  },
  {
    Header: "Time",
    accessor: "time",
  },
  {
    Header: "Date",
    accessor: "date",
  },
];
