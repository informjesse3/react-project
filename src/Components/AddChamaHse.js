import React, { useState } from 'react'
import { Link } from 'react-router-dom';

function AddChamaHse() {

  const [name, setName] = useState('')
  const [location, setLocation] = useState('')

function handleSubmit(e){
  e.preventDefault();
  setName(e.target.value)
  setLocation(e.target.value);

  console.log(name);
  console.log(location);
} 

  return (
    <div className="form">
      <h2>+Add Chama House</h2>
      <Link to="/">
        <p>Back home</p>
      </Link>
      <form onSubmit={handleSubmit}>
        <div className="input">
          <label>Name: </label>
          <input type="text" value={name} onChange={(e) => setName(e.target.value)}/>
        </div>

        <div className="input">
          <label>Loaction: </label>
          <input type="text" value={location} onChange={(e) => setLocation(e.target.value)}/>
        </div>
        <button type="submit">Submit</button>
      </form>
    </div>
  );
}

export default AddChamaHse