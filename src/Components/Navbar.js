import React from 'react'
import { Outlet, Link } from 'react-router-dom';
function Navbar() {
  return (
    <div className='navbar'>
        
      {" "}
      <nav className='links'>
        <Link to="chamahse">Link1</Link>
        <Link to="newmember">Link2</Link>
        <Link to="maintable">Link3</Link>
      </nav>
      <main>
        <Outlet />
      </main>
    </div>
  );
}

export default Navbar