import { useState } from "react";
import { Link } from "react-router-dom";

function AddnewRecForm() {
  const [member, setMember] = useState("");
  const [month, setMonth] = useState("");
  const [chamaHse, setChamaHse] = useState("");
  const [savings, setSavings] = useState("");
  const [loan, setLoan] = useState("");
  const [loanPay, setLoanPay] = useState("");
  const [isPending, setIspending] = useState(false);

  const handleSubmit = (e) => {
    e.preventDefault();
    setIspending(true);

    setMember(e.target.value);
    setMonth(e.target.value);
    setChamaHse(e.target.value);
    setSavings(e.target.value);
    setLoan(e.target.value);
    setLoanPay(e.target.value);

    console.log(member);
    console.log(month);
    console.log(chamaHse);
    console.log(savings);
    console.log(loan);
    console.log(loanPay);
    console.log("new blog added");
    setIspending(false);
  };
  return (
    <div className="form">
      <h2>+Add New Record</h2>
     
      <form onSubmit={handleSubmit}>
        <div className="input">
          <label>Member name:</label>
          <select value={member} onChange={(e) => setMember(e.target.value)}>
            <option value="mmber1" placeholder="insa">
              Julia
            </option>
            <option value="member2">Jenifer</option>
          </select>
        </div>
        <div className="input">
          <label>Month: </label>
          <select value={month} onChange={(e) => setMonth(e.target.value)}>
            <option value="Jan">January</option>
            <option value="Dec">December</option>
          </select>
        </div>
        <div className="input">
          <label>Chama house:</label>
          <select
            value={chamaHse}
            onChange={(e) => setChamaHse(e.target.value)}
          >
            <option value="Hse1">Julia Otieno</option>
            <option value="Hse2">Jenifer Otieno</option>
          </select>
        </div>
        <div className="input">
          <label>Savings Ksh: </label>
          <input
            type="number"
            value={savings}
            onChange={(e) => setSavings(e.target.value)}
          />
        </div>
        <div className="input">
          <label>Loan Ksh: </label>
          <input
            type="number"
            value={loan}
            onChange={(e) => setLoan(e.target.value)}
          />
        </div>

        <div className="input">
          <label>Loan pay Ksh: </label>
          <input
            type="number"
            value={loanPay}
            onChange={(e) => setLoanPay(e.target.value)}
          />
        </div>
        {!isPending && <button type="submit">Submit</button>}
        {isPending && <button>Adding member...</button>}
      </form>
    </div>
  );
}

export default AddnewRecForm;
