import {
  Route,
  createBrowserRouter,
  createRoutesFromElements,
  RouterProvider,
} from "react-router-dom";
import "./App.css";

// components
import AddChamaHse from "./Components/AddChamaHse";
import AddnewMember from "./Components/AddnewMember";
import AddnewRecForm from "./Components/AddnewRecForm";
import Navbar from "./Components/Navbar";
import MainTable from "./Components/Tables/MainTable";


const router = createBrowserRouter(
  createRoutesFromElements(
    <Route path="/" element={<Navbar/>}>
      <Route index element={<AddnewRecForm />} />
      <Route path="chamahse" element={<AddChamaHse />} />
      <Route path="newmember" element={<AddnewMember />} />
      <Route path="maintable" element={<MainTable />} />   
      
    </Route>
  )
);

function App() {
  return (
    
          <RouterProvider router={router}/>
  );
}

export default App;
